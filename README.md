# Front End Developer Practical Test



## Getting started

Hello Candidate, 

Welcome to Cupidknot Practical Exam.


Front End Design & Development Assessment

# *Note: Use HTML, CSS(bootstrap) to design and develop below page with responsive UI.
Page Design link: https://gitlab.com/piyush_ck/front-end-developer/-/blob/main/Desktop_-_6.jpg


IMP NOTES:

- After completing the task create public repository in GitHub or Gitlab or Bitbucket and push the code.
- If you don't know about git then you can send zip in mail attachment.
- Code should be well formatted and follows standard guidelines


